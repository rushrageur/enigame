from datetime import datetime
import base64
import random
import string

puzzlenames = [] 
puzzles = {}
SPLIT_CHARAKTER = ","
FILENAME = "enifile.txt"
IS_ENCODED = True
SHOULD_ENCODE = True
KEY = None

def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc).encode()).decode()

def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc).decode()
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)
    
def new_puzzle():
    is_name_new = False
    while not is_name_new:
        puzzle_name = input("Enter the name of the puzzle: ")
        if puzzle_name.lower() in puzzlenames:
            print("Name already exists, try again")
        else:
            is_name_new = True
    puzzlenames.append(puzzle_name.lower())
    solution = input("Enter the solution: ")
    eurekas = []
    while 1:
        puzzle_item = input("Type [stop] or enter an Eureka: ")
        if puzzle_item.lower() != "stop":
            eurekas.append(puzzle_item.lower())
        else:
            break
    puzzles[puzzle_name.lower()] = Puzzle(puzzle_name.lower(),solution.lower(),eurekas)
    #should create object Puzzle with eurekas
    print("Puzzle added. All puzzles:",puzzlenames)

    
def see_puzzles():
    print(puzzlenames)
    

def solve(puzzle_name):
    display_text = "What is the solution to riddle " + puzzle_name + "? Or type [exit] to move to all puzzles. \n"
    puzzle = puzzles.get(puzzle_name)
    while 1:
        guess = input(display_text)
        guess = guess.lower()
        if guess == puzzle.solution:
            print("That is correct. You solved puzzle", puzzle_name)
            return
        elif guess in puzzle.eurekas:
            print(guess, "is an Eureka. You're on the right track. Keep going!")
        elif guess == "exit":
            print("Come back to this puzzle if you have a solution.")
            return
        else:
            print("Wrong answer. Think about it more.")

def transform_to_output(puzzle_name):
    puzzle = puzzles.get(puzzle_name)
    text = puzzle.name + SPLIT_CHARAKTER + puzzle.solution 
    for eureka in puzzle.eurekas:
        text += SPLIT_CHARAKTER + eureka
    return text
    
def read_file():
    f = open(FILENAME, "r")
    lines = f.readlines()
    print("reading state from", lines[0][0:20], "puzzles to be read", lines[0][40:])
    global KEY
    KEY = lines[1].strip()
    info = lines[2:]
    for line in info:
        extract_riddle(line.strip())
    print("import complete")
    f.close()
 
def extract_riddle(line):
    if IS_ENCODED:
        line = decode(KEY,line)
    puzzle_stuff = line.split(SPLIT_CHARAKTER)
    puzzle_name = puzzle_stuff[0]
    puzzle_solution = puzzle_stuff[1]
    puzzle_eurekas = puzzle_stuff[2:]
    puzzlenames.append(puzzle_name.lower())
    puzzles[puzzle_name] = Puzzle(puzzle_name,puzzle_solution,puzzle_eurekas)
    print("Puzzle", puzzle_name ,"read from file.")

def generate_key():
    N = random.randint(8, 14)
    key = ''.join(random.choices(string.ascii_lowercase + string.ascii_uppercase + string.digits, k=N))
    if len(key) %3 == 1:
        key += "=="
    elif len(key) %3 == 2:
        key += "="
    global KEY
    KEY = key
    #print("new key",KEY)
    
    
 
def write_to_file():
    f = open(FILENAME, "w")
    date_time = datetime.now().strftime("%d.%m.%Y, %H:%M:%S")
    generate_key()
    #print(KEY)
    output = date_time + " number of puzzles: " + str(len(puzzles)) +"\n" + KEY
    f.write(output)
    for puzzle in puzzles:
        text = "\n" 
        if SHOULD_ENCODE:
            text += encode(KEY,transform_to_output(puzzle))
        else:
            text += transform_to_output(puzzle)
        f.write(text)
    f.close()
    print("State safed in", FILENAME)
    

    
class Puzzle:
  def __init__(self, name, solution, eurekas):
    self.name = name
    self.solution = solution
    self.eurekas = eurekas

def main():
    read_file()
    #read state
    while 1:
        what = input("\nDo you want to set up a new [puzzle] oder see the [list]? \nType [exit] to exit or the puzzle name to solve.\n")
        if what == "exit":
            write_to_file()
            break
        elif what.lower() == "puzzle":
            new_puzzle()
        elif what.lower() == "list":
            see_puzzles()
        elif what.lower() in puzzlenames:
            solve(what)
        else:
            print("Could not read your input. There is no puzzle named",what)


if __name__ == "__main__":
    main()

